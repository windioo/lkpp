<?php 
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://api.binderbyte.com/v1/track?api_key=a6db4a4cc7812b98c4ef7a03ec1c6aacc6e4781adfb6827eb844716795b77b7c&courier=jne&awb=8825112045716759');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec($curl);
curl_close($curl);
$result = json_decode($result, true);

$awb = $result['data']['summary']['awb'];
$tanggalpengiriman = $result['data']['history'][1]['date'];
$pengirim = $result['data']['detail']['shipper'];
$penerima = $result['data']['detail']['receiver'];
$courier = $result['data']['summary']['courier'];
$date = $result['data']['summary']['date'];
$asal = $result['data']['detail']['origin'];
$tujuan = $result['data']['detail']['destination'];
$service = $result['data']['summary']['service'];
$history =$result['data']['history'] 
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

    <title>CEK PAKET</title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">CEK PAKET</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-link active" aria-current="page" href="#">Home</a>
      </div>
    </div>
  </div>
</nav>

<div class="container">
    <div class="container">
      <div class="row mt-3">
        <div class="col-md-11 ">
          <h1 class="text-center mb-5">Cek Paket</h1>
          <div class="input-group mb-4">
            <input
              id="search-input"
              type="text"
              class="form-control rounded-pill"
              placeholder="Input resi anda ..."
              aria-label="Recipient's username"
              aria-describedby="basic-addon2"
            />
            <span
              id="search-button"
              class="input-group-text btn-dark rounded-pill ms-3 bg-primary"
              id="basic-addon2"
              >Search</span
            >
          </div>
        </div>
      </div>
      <div class="row ms-5">
          <div class="col">
              <input type="radio" name="JNE" id=""> JNE
          </div>
          <div class="col">
               <input type="radio" name="TIKI" id=""> TIKI
          </div>
          <div class="col">
              <input type="radio" name="POS" id=""> POS
          </div>
          <div class="col">
              <input type="radio" name="SiCepat" id=""> SiCepat
          </div>
          <div class="col">
              <input type="radio" name="AnterAja" id=""> AnterAja
          </div>
          <div class="col">
              <input type="radio" name="Ninja" id=""> Ninja
          </div>
          <div class="col">
               <input type="radio" name="JNT" id=""> JNT  
          </div>
      </div>
      <h4 id="keterangan"></h4>

      <hr />
      

      <div class="row mt-3 text-center ">
           <div class="col-md-3">
              <h4>Nomor Resi</h4>
          </div>
          <div class="col-md-3">
               <h4>Tanggal Pengiriman</h4>
          </div>
          <div class="col-md-3">
              <h4>Pengirim</h4>
          </div>
          <div class="col-md-3">
              <h4>Penerima</h4>
          </div>
      </div>
      <div class="row mt-3 text-center ">
           <div class="col-md-3">
              <p id="no-resi"><?=$awb; ?></p>
          </div>
          <div class="col-md-3">
               <p id="tanggal-pengiriman"><?= $tanggalpengiriman; ?></p>
          </div>
          <div class="col-md-3">
              <p id="pengirim"><?= $pengirim;?></p>
          </div>
          <div class="col-md-3">
              <p id="penerima"><?= $penerima;?></p>
          </div>
      </div>
        <div class="row mt-3 text-center">
           <div class="col-md-3">
              <p id="no-resi"><?= $courier;?></p>
          </div>
          <div class="col-md-3">
               <p id="tanggal-pengiriman"><?= $date;?></p>
          </div>
          <div class="col-md-3">
              <p id="pengirim"><?= $asal;?></p>
          </div>
          <div class="col-md-3">
              <p id="penerima"><?= $tujuan;?></p>
          </div>
      </div>
        <div class="row mt-3 text-center">
           <div class="col-md-3">
              <p id="no-resi"><?= $service;?></p>
          </div>
         
      </div>
           
     
      <hr>
      <h3 class="ms-5">Detil Pengiriman</h3>
      <div class="row mt-5 text-center ">
           <div class="col-md-3">
              <h4>Tanggal/Waktu</h4>
          </div>
          <div class="col-md-3">
              <h4>Deskripsi</h4>
          </div>
      </div>
      <?php foreach ($history as $ht) : ?>
        <div class="row mt-5 text-center ">
           <div class="col-md-3">
              <p id="tanggal"><?= $ht['date'];?></p>
          </div>
          <div class="col-md-3">
              <p id="deskripsi"><?= $ht['desc'];?></p>
          </div>
      </div>
      <?php endforeach ?>
      </div>
    </div>
</div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

  </body>
</html>